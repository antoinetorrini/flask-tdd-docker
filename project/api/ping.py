from flask_restplus import Namespace, Resource

ping_namespace = Namespace("ping")


class Ping(Resource):
    def get(self):
        return {"status": "success with CI/CD", "message": "pong!"}


ping_namespace.add_resource(Ping, "")
