from flask import request
from flask_restplus import Namespace, Resource, fields

from project.api.services import (
    add_user,
    delete_user,
    get_all_users,
    get_user_by_email,
    get_user_by_id,
    update_user,
)

users_namespace = Namespace("users")

user = users_namespace.model(
    "User",
    {
        "id": fields.Integer(readOnly=True),
        "username": fields.String(required=True),
        "email": fields.String(required=True),
        "created_date": fields.DateTime,
    },
)


class Users(Resource):
    @users_namespace.marshal_with(user)
    @users_namespace.response(200, "Success")
    @users_namespace.response(404, "User <user_id> does not exist")
    def get(self, user_id):
        """Returns a single user."""
        user = get_user_by_id(user_id)

        # validation on user
        if not user:
            users_namespace.abort(404, f"User {user_id} does not exist")

        return user, 200

    @users_namespace.response(200, "<user_is> was removed!")
    @users_namespace.response(404, "User <user_id> does not exist")
    def delete(self, user_id):
        """Delete a user."""
        response_object = {}
        user = get_user_by_id(user_id)

        # validation on user
        if not user:
            users_namespace.abort(404, f"User {user_id} does not exist")

        # commit to db
        delete_user(user)

        response_object["message"] = f"{user.email} was removed!"

        return response_object, 200

    @users_namespace.response(200, "<user_is> was updated!")
    @users_namespace.response(404, "User <user_id> does not exist")
    @users_namespace.expect(user, validate=True)
    def put(self, user_id):
        """Updates a user."""
        # getting data
        post_data = request.get_json()
        username = post_data.get("username")
        email = post_data.get("email")
        response_object = {}

        # validation on user
        user = get_user_by_id(user_id)
        if not user:
            users_namespace.abort(404, f"User {user_id} does not exist")

        # commit to db
        update_user(user, username, email)

        response_object["message"] = f"{user.id} was updated!"
        return response_object, 200


class UsersList(Resource):
    @users_namespace.marshal_with(user, as_list=True)
    def get(self):
        """Get all users."""
        return get_all_users(), 200

    @users_namespace.expect(user, validate=True)
    @users_namespace.response(201, "<user_email> was added!")
    @users_namespace.response(400, "Sorry. That email already exists.")
    def post(self):
        """Create a new user."""
        # getting data
        post_data = request.get_json()
        username = post_data.get("username")
        email = post_data.get("email")
        response_object = {}

        # input payload validation
        user = get_user_by_email(email=email)
        if user:
            response_object["message"] = "Sorry. That email already exists."
            return response_object, 400

        # commit to db
        add_user(username=username, email=email)

        # response object
        response_object = {"message": f"{email} was added!"}

        return response_object, 201


users_namespace.add_resource(UsersList, "")
users_namespace.add_resource(Users, "/<int:user_id>")
