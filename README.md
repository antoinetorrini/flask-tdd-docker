# Flask TDD Docker course

[![pipeline status](https://gitlab.com/antoinetorrini/flask-tdd-docker/badges/master/pipeline.svg)](https://gitlab.com/antoinetorrini/flask-tdd-docker/commits/master)

# Useful commands

To build the images run 

```bash
docker-compose build
```

To fire up the containers in detached mode run

```bash
docker-compose up -d
```

Add the ``--build`` argument to re build the containers.

### Running tests

```bash
docker-compose exec users pytest "project/tests"
```